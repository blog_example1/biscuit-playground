# Get sources from git
rm -fr biscuit-component-wasm
rm -fr biscuit-web-components
git clone --branch add-generate-public-key-from-private https://github.com/Akanoa/biscuit-component-wasm.git
git clone --branch base64-playground https://github.com/Akanoa/biscuit-web-components.git

# Install Rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
source "$HOME/.cargo/env"

# Build wasm bindding
cd biscuit-component-wasm 
npx -y wasm-pack build --scope biscuit-auth --target web --out-dir web --out-name biscuit

cd web 
yarn link

# Build Web Component
cd ../../biscuit-web-components
yarn link "@biscuit-auth/biscuit-wasm-support"
yarn
yarn run build
yarn link

# Build sources
cd  ..
yarn
yarn link "@biscuit-auth/web-components"
yarn run build

