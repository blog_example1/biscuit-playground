import {initialize} from "@biscuit-auth/web-components";
export {Configuration} from "@biscuit-auth/web-components";

async function setup() {
    await initialize()
}

await setup()