import nodeResolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { importMetaAssets } from '@web/rollup-plugin-import-meta-assets';
import copy from 'rollup-plugin-copy'

export default {
    input: 'index.js',
    output: {
        dir: 'public',
        format: 'esm'
    },
    plugins: [
        copy({
            targets: [
                {src: "examples/*.html", dest: "public/"},
                {src: "index.html", dest: "public/"},
            ]
        }),
        nodeResolve({ browser: true }),
        commonjs({
            include: 'node_modules/**'
        }),
        importMetaAssets()
    ]
};